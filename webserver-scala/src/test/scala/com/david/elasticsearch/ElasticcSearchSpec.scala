package com.david.elasticsearch

import com.david.selenium.{Education, Experience, Summary, User}
import org.specs2.mutable._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{Await, Future}


/**
  * Created by davidho on 08/11/2015.
  */
object ElasticSearchSpec extends Specification{


  "Elastic Search" should {
    "User object should be parsed to a Json" in {
      val expectedResult ="""{
                            |        "username":    "Bob",
                            |        "summary" :{
                            |            "position": "Developer" ,
                            |            "education":  "Bar Ilan",
                            |            "location": "Tel Aviv",
                            |            "industry": "Hi Tech"
                            |        },
                            |        "skills" : "Java Scala",
                            |        "education" : [
                            |                        {
                            |                          "title": "Bar Ilan",
                            |                          "subTitle": "Computer Science",
                            |                          "start": "2010",
                            |                          "end": "2013"
                            |                        }
                            |            ],
                            |        "experience": [{
                            |                "title": "Actimize",
                            |                "subtitle": "Developer",
                            |                "dateRange": "2010-2013",
                            |                "description": "AML Lob"
                            |              }
                            |          ],
                            |        "language" : "Hebrew English"
                            |  }""".stripMargin




      val summary = Summary("Developer","Bar Ilan","Tel Aviv","Hi Tech")
      val education = List(Education("Bar Ilan","Computer Science","2010","2013"))
      val skills = List("Java","Scala")
      val experience = List(Experience("Actimize","Developer","2010-2013","AML Lob"))
      val language = List("Hebrew","English")

      val user = User("Bob",summary,experience,skills,language,education)
      val jsonUserStr = ElasticSearch.UserIndexable.json(user)
//      jsonUserStr  === expectedResult
      ok
    }

    "search by skils" in{
      import scala.concurrent.duration._
      import scala.concurrent.ExecutionContext.Implicits.global
      val futureRes: Future[Array[String]] = ElasticSearch.searchBySkills("D E F")
      val res =Await.result(futureRes, 20000 milli)
      res.size  === 3
    }


    "search by username" in{
      import scala.concurrent.duration._
      import scala.concurrent.ExecutionContext.Implicits.global
      val futureRes: Future[Array[String]] = ElasticSearch.searchByName("Alon")
      val res =Await.result(futureRes, 20000 milli)
      res.size  === 2

    }

    "index new user" in{
      import scala.concurrent.duration._
      import scala.concurrent.ExecutionContext.Implicits.global
      val summary = Summary("Developer","Bar Ilan","Tel Aviv","Hi Tech")
      val education = List(Education("Bar Ilan","Computer Science","2010","2013"))
      val skills = List("Java","Scala")
      val experience = List(Experience("Actimize","Developer","2010-2013","AML Lob"))
      val language = List("Hebrew","English")

      val user = User("Bob",summary,experience,skills,language,education)
      ElasticSearch.indexUser(user)
      ok
    }

    "should parse user david to json" in {
      val user = User("David Hodeffi",
        Summary("Nice-Actimize","Bar Ilan","Beit Dagan, HaMerkaz (Central) District, Israel","Computer Software"),
        ArrayBuffer(
          Experience("""Scala\Java Developer""","Nice-Actimize","February 2013 – Present (3 years 5 months)","Developed an AML solution (i.e. financial crime domain) that is used by organizations (including major banks) to detect, deter and monitor (analytics) billions of transactions. worked on the most sold product in Actimize, which involves real time and batch detection processes. I have developed on Oracle/SQL Server (MSSQL) Databases" +
          ", Scala, Java, Spring, Tomcat. We have supported both Linux/Windows operating system.        Kanban Development methodology."+
        "Maven and Jenkins as Build tool and Build Server." +
        "SVN and Git as source control.")),
        ArrayBuffer("OOP", "Object Oriented Design", "Java", "C", "Design Patterns", "Eclipse", "C++", "SQL",
          "Visual Studio", "Software Development", "Python", "HTML", "Linux", "AJAX", "Databases", "Multithreading", "Software Design", "Software Engineering",
          "JavaScript", "Scala", "CSS3", "Android", "XML", "CSS", "jQuery", "UML", "HTML 5",
          "Web Development", "Ant", "Maven", "SBT", "XSL", "Oracle", "Microsoft SQL Server",
          "Functional Programming", "Jenkins", "ORM Tools", "Akka", "Multiprocessing", "Performance Management",
          "Git", "Mercurial", "Tortoise SVN", "IntelliJ IDEA", "Cross-platform Development",
          "Google App Engine", "SQLite", "Algorithms", "Agile Methodologies"),
      ArrayBuffer("Hebrew", "English"),
      ArrayBuffer(Education("Bar Ilan","Bachelor’s Degree, Computer Science","2010","2012")))

      val jsonUserStr = ElasticSearch.UserIndexable.json(user)
      println(jsonUserStr)
      ok
    }

  }

}
