package com.david

import akka.actor.ActorSystem
import org.specs2.mutable.Specification
import spray.http.StatusCodes._
import spray.testkit.Specs2RouteTest

object Timeouts{
  import concurrent.duration._
  import akka.testkit._
  implicit def myDefault(implicit system: ActorSystem) = 250.second dilated
}

class MyServiceSpec extends Specification with Specs2RouteTest with MyService{
  def actorRefFactory = system


  implicit def default(implicit system: ActorSystem) = RouteTestTimeout(Timeouts.myDefault)
  "MyService" should {

    "return a greeting for GET requests to the root path" in {
      Get() ~> myRoute ~> check {
        responseAs[String] must contain("""<h1>Welcome to My Application!</h1>""")
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get("/kermit") ~> myRoute ~> check {
        handled must beFalse
      }
    }

    "return a MethodNotAllowed error for PUT requests to the root path" in {
      Put() ~> sealRoute(myRoute) ~> check {
        status === MethodNotAllowed
        responseAs[String] === "HTTP method not allowed, supported methods: GET"
      }
    }


    "GET search by username request should be handled" in {
      Get("/searchbyusername?username=Alon") ~> myRoute ~> check {
        println("search by username response is " + responseAs[String])
        handled must beTrue
      }
    }


    "GET search by skill request should be handled" in {
      Get("/searchbyskills?skills=A%20D%20F") ~> myRoute ~> check {
        println("search by skill response is " + responseAs[String])
        handled must beTrue
      }
    }

    "Get for indexing linkedin profile should be handled" in {
      Get("/indexprofile?url=https://www.linkedin.com/in/elia-grady-90ab9654") ~> myRoute ~> check {
        println("search by skill response is " + responseAs[String])
        handled must beTrue
      }

      ok
    }

  }


}

