package com.netaporter.routing

import akka.actor._
import akka.actor.SupervisorStrategy.Stop
import spray.http.StatusCodes._
import spray.routing.RequestContext
import akka.actor.OneForOneStrategy
import spray.httpx.Json4sSupport
import scala.concurrent.duration._
import org.json4s.DefaultFormats
import spray.http.StatusCode
import com.netaporter._
import com.netaporter.routing.PerRequest._

trait RestMessage

case class Error(message: String)

case class Validation(message: String)

/**
  * An Abstraction for handling a connection per web service request
  */
trait PerRequest extends Actor with Json4sSupport {

  import context._


  val json4sFormats = DefaultFormats

  def r: RequestContext
  def target: ActorRef
  def message: RestMessage

  setReceiveTimeout(60.seconds)
  target ! message

  def receive = {
    case res: RestMessage => complete(OK, res)
    case v: Validation    => complete(BadRequest, v)
    case ReceiveTimeout   => complete(GatewayTimeout, Error("Request timeout"))
  }

  def complete[T <: AnyRef](status: StatusCode, obj: T) = {
    r.complete(status, obj)
    stop(self)
  }

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e => {
        complete(InternalServerError, Error(e.getMessage))
        Stop
      }
    }
}

object PerRequest {
  case class WithActorRef(r: RequestContext, target: ActorRef, message: RestMessage) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: RestMessage) extends PerRequest {
    lazy val target = context.actorOf(props)
  }
}

trait PerRequestCreator {

  def perRequest(actorRefFactory: ActorRefFactory,r: RequestContext, target: ActorRef, message: RestMessage) =
    actorRefFactory.actorOf(Props(new WithActorRef(r, target, message)))

  def perRequest(actorRefFactory: ActorRefFactory,r: RequestContext, props: Props, message: RestMessage) =
    actorRefFactory.actorOf(Props(new WithProps(r, props, message)))
}