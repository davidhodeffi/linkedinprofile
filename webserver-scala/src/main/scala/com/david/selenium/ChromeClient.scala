package com.david.selenium

import org.openqa.selenium.{By, WebElement}
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver

import scala.util.Try

/**
  * Created by David on 17/06/2016.
  * A client that parse a linkedin profile.
  * Parsing is processed using selenium because linkedin block direct http request.
  */
object ChromeClient{


  def parseLinkedinProfile(url: String) = {
    import scala.collection.JavaConverters._
    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    val driver = new ChromeDriver()
    implicit def TryToEmptyStr(value:Try[WebElement])= value.map(_.getText).getOrElse("")
    try {
      driver.get(url)
      val nameElement = Try(driver.findElementByCssSelector("#name"))
      val titleElement = Try(driver.findElementByCssSelector("#topcard > div.profile-card.vcard.no-picture > div > div > p > span"))
      val locationElement = Try(driver.findElementByCssSelector("#demographics > dd.descriptor.adr > span"))
      val educationElement = Try(driver.findElementByCssSelector("#topcard > div.profile-card.vcard.no-picture > div > div > table > tbody > tr:nth-child(2) > td > ol > li"))
      val industryElement = Try(driver.findElementByCssSelector("#demographics > dd:nth-child(4)"))
      val currentPositionElement =
        Try(driver.findElementByCssSelector("#topcard > div.profile-card.vcard.no-picture > div > div > table > tbody > tr:nth-child(1) > td > ol > li > span > a"))
      val skillsElements = driver.findElementsByCssSelector("#skills > ul > li > a > span").asScala
      val languageElements = driver.findElementsByCssSelector("#languages > ul > li > div > h4 > span").asScala
      val experienceElement = driver.findElementsByCssSelector("#experience > ul > li").asScala
      val educationElements = driver.findElementsByCssSelector("#education > ul > li").asScala
      //using attribute innerText because of a chrome driver
      val skills = skillsElements map { skill => skill.getAttribute("innerText") } toSeq

      def parseExperience(webElement: WebElement) = {
        val title = Try(webElement.findElement(By.cssSelector(".item-title > a > span")))
        val subtitle = Try(webElement.findElement(By.cssSelector(".item-subtitle > a > span")))
        val dateRange = Try(webElement.findElement(By.cssSelector(".date-range")))
        val description = Try(webElement.findElement(By.cssSelector(".description")))
        Experience(title, subtitle, dateRange, description)
      }


      def parseEducation(webElement: WebElement) = {
        val title = Try(webElement.findElement(By.cssSelector(".item-title >  span")))
        val subtitle = Try(webElement.findElement(By.cssSelector(".item-subtitle >  span")))
        val date = webElement.findElements(By.cssSelector(".date-range > time")).asScala.map(_.getText).toList
        val start = date(0)
        val end = date(1 )
        Education(title, subtitle, start, end)

      }
      val languages = languageElements.map(language => language.getText).toSeq
      val experiences = experienceElement.map(parseExperience).toSeq
      val educations = educationElements map parseEducation toSeq





      val summary = Summary(currentPositionElement,
        educationElement, locationElement, industryElement)
      val user = User(nameElement, summary: Summary, experiences, skills, languages, educations)
      user


    } finally {
      driver.close()
    }
  }

}


case class Education(title: String, subTitle: String, start: String, end: String)

case class Experience(title: String, subtitle: String, dateRange: String, description: String)

case class Summary(currentPosition: String, education: String, location: String, industry: String)

case class User(name: String, summary: Summary, experience: Seq[Experience], skills: Seq[String], languages: Seq[String], education: Seq[Education])


/**
  * An Example of how a linkedin profile can be processed.
  */
object ChromeClientClientssMain {

  def main(args: Array[String]) {
    println(ChromeClient.parseLinkedinProfile("https://www.linkedin.com/in/elia-grady-90ab9654"))
  }
}
