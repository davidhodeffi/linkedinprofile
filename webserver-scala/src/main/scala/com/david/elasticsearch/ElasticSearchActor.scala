package com.david.elasticsearch

import java.util.concurrent.Executor

import akka.actor.{Actor, ActorLogging}
import com.david.selenium.User
import com.netaporter.routing.{RestMessage, Validation}
import com.sksamuel.elastic4s.BulkResult

import scala.concurrent.ExecutionContext

/**
  * Created by davidho on 13/11/2015.
  */
class ElasticSearchActor extends Actor with ActorLogging {

  import ElasticSearchActor._
  import akka.pattern.pipe

  override def receive: Receive = {

    case IndexUser(user: User) =>
      log.debug("indexing user {}", user)
      implicit val executor = context.dispatcher.asInstanceOf[Executor with ExecutionContext]
      val response = ElasticSearch.indexUser(user).map(x => IndexUserResult(x))
      response pipeTo self

    case SearchLinkedinBySkills(skills) =>
      implicit val executor = context.dispatcher.asInstanceOf[Executor with ExecutionContext]
      val response = ElasticSearch.searchBySkills(skills).map(SearchResult(_))
      response pipeTo self

    case SearchLinkedinByName(name) =>
      implicit val executor = context.dispatcher.asInstanceOf[Executor with ExecutionContext]
      val response = ElasticSearch.searchByName(name).map(SearchResult(_))
      response pipeTo self

    case x: SearchResult =>
      context.parent ! x
      context.stop(self)

    case x: IndexUserResult =>
      context.parent ! x
      context.stop(self)

    case f: Validation => context.parent ! f
  }


}


object ElasticSearchActor {

  case class IndexUser(user: User) extends RestMessage

  case class SearchLinkedinBySkills(skills: String) extends RestMessage

  case class SearchLinkedinByName(name: String) extends RestMessage

  case class SearchResult(result: Array[String]) extends RestMessage

  case class IndexUserResult(result: BulkResult) extends RestMessage

}
