package com.david.elasticsearch

import com.david.selenium.User
import com.sksamuel.elastic4s.ElasticDsl.{index, _}
import com.sksamuel.elastic4s.source.Indexable
import com.sksamuel.elastic4s.{BulkResult, ElasticClient, ElasticsearchClientUri}
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by davidho on 11/11/2015.
  * An object with api to index queries or search queries.
  */
object ElasticSearch {
  private val uri = ElasticsearchClientUri("elasticsearch://localhost:9300")
  private val client = ElasticClient.remote(uri)

  implicit object UserIndexable extends Indexable[User] {
    override def json(u: User): String = {
      s"""{
        "username":    "${forJSON(u.name)}",
        "summary" :{
            "position": "${forJSON(u.summary.currentPosition)}" ,
            "education":  "${forJSON(u.summary.education)}",
            "location": "${forJSON(u.summary.location)}",
            "industry": "${forJSON(u.summary.industry)}"
        },
        "skills" : "${forJSON(u.skills.mkString(" "))}",
        "education" : [${
        u.education.map { education => {
          s"""
                        {
                          "title": "${forJSON(education.title)}",
                          "subTitle": "${forJSON(education.subTitle)}",
                          "start": "${forJSON(education.start)}",
                          "end": "${forJSON(education.end)}"
                        }"""
        }
        }.mkString(",")
      }
            ],
        "experience": [${
        u.experience.map { job =>
          s"""{
                "title": "${forJSON(job.title)}",
                "subtitle": "${forJSON(job.subtitle)}",
                "dateRange": "${forJSON(job.dateRange)}",
                "description": "${forJSON(job.description)}"
              }"""
        }.mkString(",")
      }
          ],
        "language" : "${forJSON(u.languages.mkString(" "))}"
  }"""


    }
  }

  /**
    * index a new user into the elastic search server
    *
    * @param user
    * @return relevant users
    */
  def indexUser(user: User): Future[BulkResult] = {

    val commands = index into "index" / "user" source user
    println(commands.show)
    val query = bulk(commands)
    client.execute(query)
  }

  /**
    * search for a user by skills. a gull text search on the 'skills' field.
    * The more relevant skills a user has, the rank would be greater
    *
    * @param skills a string with the skills i.e "Java Oracle elasticsearch"
    * @param e      execution context
    * @return
    */
  def searchBySkills(skills: String)(implicit e: ExecutionContext) = {
    val myQuery = search in "index" / "user" rawQuery {
      s"""{
          |  "query": {
          |    "match": {
          |      "skills": "${skills}"
          |    }
          |  }
          |}""".stripMargin
    }
    println(myQuery.show)
    val response = client.execute(myQuery)
    import scala.collection.JavaConverters._
    val res = response.map(_.getHits.getHits.map(x => parseDocument(x.getSource.asScala.toMap)))
    res
  }

  /**
    * search a user by username. a full text search. i.e the more specific the user the score is greater
    *
    * @param username the user name i.e "David Hodeff"
    * @param e        implicit execution context
    * @return escaped string
    */
  def searchByName(username: String)(implicit e: ExecutionContext) = {
    val myQuery = search in "index" / "user" rawQuery {
      s"""{
          |  "query": {
          |    "match": {
          |      "username": "${username}"
          |    }
          |  }
          |}""".stripMargin
    }
    println(myQuery.show)
    val response = client.execute(myQuery)
    import scala.collection.JavaConverters._
    val res = response.map(_.getHits.getHits.map(x => parseDocument(x.getSource.asScala.toMap)))
    res
  }


  def parseDocument(map: Map[String, AnyRef]) = map.toString

  /**
    * string escaping for Json format
    * @param aText
    * @return
    */
  def forJSON(aText: String) = {
    val result = new StringBuilder();
    val characters = aText.toCharArray
    characters foreach { character =>
      if (character == '\"') {
        result.append("\\\"");
      }
      else if (character == '\\') {
        result.append("\\\\");
      }
      else if (character == '/') {
        result.append("\\/");
      }
      else if (character == '\b') {
        result.append("\\b");
      }
      else if (character == '\f') {
        result.append("\\f");
      }
      else if (character == '\n') {
        result.append("\\n");
      }
      else if (character == '\r') {
        result.append("\\r");
      }
      else if (character == '\t') {
        result.append("\\t");
      }
      else {
        //the char is not a special one
        //add it to the result as is
        result.append(character);
      }
    }
    result.toString();
  }
}
