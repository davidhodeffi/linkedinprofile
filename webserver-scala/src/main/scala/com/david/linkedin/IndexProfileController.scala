package com.david.linkedin

/**
  * Created by davidho on 08/11/2015.
  */

import akka.actor.{Actor, ActorLogging, ActorRef, Props, ReceiveTimeout}
import com.david.elasticsearch.ElasticSearchActor
import com.david.elasticsearch.ElasticSearchActor.IndexUser

import scala.concurrent.duration._

object IndexProfileController {

  case class ParseLinkedinProfile(url: String)

  case class Result(user: String)

  case object UrlRemoved

  case object Abort

}

/**
  * Controller Actor for Indexing profile.
  * He Orchestrate connection between multiple Actors i.e Crawling Actor and Indexing Actor
  */
class IndexProfileController extends Actor with ActorLogging {

  import IndexProfileController._

  var childrenGetters = Set.empty[ActorRef]

  context.setReceiveTimeout(60.seconds)

  def receive = {
    case ParseLinkedinProfile(url) =>
      log.debug("parsing profile at url {}", url)
      childrenGetters += context.actorOf(Props(new LinkedinProfileCrawler(url)))


    case LinkedinProfileCrawler.Done(user) =>
      log.debug("indexing linkedin user:\n {}", user)
      childrenGetters -= sender
      val indexerActor = context.actorOf(Props(new ElasticSearchActor))
      childrenGetters += indexerActor
      indexerActor ! IndexUser(user)

    case ElasticSearchActor.IndexUserResult(result) =>
      log.debug(s"indexer result:\n $result")
      context.parent ! Receptionist.IndexProfileResult(result.toString)
      context stop self

    case ReceiveTimeout =>
      context.children foreach (_ ! Abort)
  }

}
