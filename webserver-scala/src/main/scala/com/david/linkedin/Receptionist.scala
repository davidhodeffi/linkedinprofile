package com.david.linkedin

/**
  * Created by davidho on 08/11/2015.
  */

import akka.actor.{Actor, ActorRef, Props}
import com.netaporter.routing.RestMessage

object Receptionist {

  private case class Job(client: ActorRef, url: String)

  case class Get(url: String) extends RestMessage

  case class IndexProfileResult(result: String) extends RestMessage

  case class Failed(url: String)

}

/**
  * The receptionist is an Actor that that recieve client messages. then he would a create a controllere fore each request.
  * He can block incoming client messages in case there is a huge load.
  */
class Receptionist extends Actor {

  import Receptionist._

  var reqNo = 0

  def receive = waiting

  val waiting: Receive = {
    case Get(url) =>
      context.become(runNext(Vector(Job(sender, url))))
  }

  def running(queue: Vector[Job]): Receive = {
    case IndexProfileResult(indexUserResult) =>
      val job = queue.head
      job.client ! IndexProfileResult(indexUserResult)
      context.stop(sender)
      context.become(runNext(queue.tail))
    case Get(url) =>
      context.become(enqueueJob(queue, Job(sender, url)))
  }

  def runNext(queue: Vector[Job]): Receive = {
    reqNo += 1
    if (queue.isEmpty) waiting
    else {
      val controller = context.actorOf(Props[IndexProfileController], s"c$reqNo")
      controller ! IndexProfileController.ParseLinkedinProfile(queue.head.url)
      running(queue)
    }
  }

  def enqueueJob(queue: Vector[Job], job: Job): Receive = {
    if (queue.size > 3) {
      sender ! Failed(job.url)
      running(queue)
    } else running(queue :+ job)
  }

}
