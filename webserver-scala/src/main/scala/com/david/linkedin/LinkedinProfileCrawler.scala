package com.david.linkedin

/**
  * Created by davidho on 08/11/2015.
  */

import java.util.concurrent.Executor

import akka.actor.{Actor, ActorLogging}
import akka.pattern.pipe
import com.david.selenium.{ChromeClient, User}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}


object LinkedinProfileCrawler {

  case class Parse(url: String)

  case class Done(user: User)

  case object Abort

}

/**
  * An Actor for parsing a web page of a linkedin profile
  *
  * @param url
  */
class LinkedinProfileCrawler(url: String) extends Actor with ActorLogging {

  import LinkedinProfileCrawler._

  implicit val executor = context.dispatcher.asInstanceOf[Executor with ExecutionContext]

  context.setReceiveTimeout(60.seconds)
  Future(ChromeClient.parseLinkedinProfile(url)) map (Done(_)) pipeTo self

  def receive = {
    case x: Done =>
      context.parent ! x
      context.stop(self)

    case _ =>
  }

}
