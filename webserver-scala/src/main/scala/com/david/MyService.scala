package com.david

import akka.actor.{Actor, Props}
import com.david.elasticsearch.ElasticSearchActor
import com.david.elasticsearch.ElasticSearchActor.{SearchLinkedinByName, SearchLinkedinBySkills}
import com.david.linkedin.Receptionist
import com.netaporter.routing.{PerRequestCreator, RestMessage}
import spray.http.MediaTypes._
import spray.routing._

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor extends Actor with MyService with PerRequestCreator {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)

}

/**
  * Definition of the rest api for the web server
  */
trait MyService extends HttpService with PerRequestCreator {

  import com.david.linkedin.Receptionist._

  val receptionist = actorRefFactory.actorOf(Props[Receptionist], "receptionist")

  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) {
          // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <h1>Welcome to My Application!</h1>
              </body>
            </html>
          }
        }
      }

    } ~path("indexprofile") {
      get {
        parameter('url.as[String]) { url =>
          handleMessage(Get(url))
        }
      }
    } ~
      path("searchbyusername") {
        get {
          parameter('username.as[String]) { username =>
            handleMessage(SearchLinkedinByName(username))
          }
        }
      } ~
      path("searchbyskills") {
        get {
          parameter('skills.as[String]) { skills =>
            handleMessage(SearchLinkedinBySkills(skills))
          }
        }
      }



  def handleMessage(message: RestMessage): Route = message match {
    case x: SearchLinkedinByName => ctx => perRequest(actorRefFactory, ctx, Props(new ElasticSearchActor()), message)
    case x: SearchLinkedinBySkills => ctx => perRequest(actorRefFactory, ctx, Props(new ElasticSearchActor()), message)
    case x:Get => ctx => perRequest(actorRefFactory,ctx, receptionist, x)

    case _ => ctx => {}
  }

}