organization  := "com.example"

version       := "0.1"

scalaVersion  := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

javacOptions ++= Seq("-source", "1.7")

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies ++= {
  val akkaV = "2.3.9"
  val sprayV = "1.3.3"
  Seq(
    "io.spray"            %%  "spray-can"     % sprayV,
    "io.spray"            %%  "spray-routing" % sprayV,
    "io.spray"            %%  "spray-testkit" % sprayV  % "test",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test",
    "org.jsoup" % "jsoup" % "1.8.3",
    "com.ning" % "async-http-client" % "1.9.31",
    "ch.qos.logback" % "logback-classic" % "1.0.7",
    "com.sksamuel.elastic4s" %% "elastic4s-streams" % "2.3.0" exclude("org.elasticsearch","elasticsearch"),
    "org.elasticsearch" % "elasticsearch" % "2.3.2" force(),
     "org.json4s" %% "json4s-native" % "3.2.11",
    "org.seleniumhq.selenium" % "selenium-firefox-driver" % "2.53.0",
    "org.seleniumhq.selenium" % "selenium-chrome-driver" % "2.53.0"
    //"org.elasticsearch" % "elasticsearch" % "1.7.3"
  )
}

Revolver.settings
