# linkedin profile crawler

### Prerequiesite
* Vagrant
* Virtual Box
* Windows OS (it is not supporting linux because of the chromedriver)
* JDK 8
* SBT 0.13


### installation
You should run the following commands for initializing elasticsearch VM
```bash
$ cd elasticsearch
$ vagrant up
```
you should run the commands from **elasticsearch_init.script** on the elastic search using curl / sense plugin
start webserver
```bash
$ sbt re-start
```

stop webserver
```bash
$ sbt re-stop
```

### usecase

example of how to use the rest api can be seen on MyServiceSpec.scala
Since linkedin blocking http request 
```bash 
curl -I --url https://www.linkedin.com/company/linkedin
```
Result : 
```bash
HTTP/1.1 999 Request denied
Date: Tue, 18 Nov 2014 23:20:48 GMT
Server: ATS
X-Li-Pop: prod-lva1
Content-Length: 956
Content-Type: text/html
```
I have hacked linkedin using selenium. Crawling a linkedin profile web page using http request is blocked based on user-agent, even if you fake your agent we would need the data that is retreived by javascript.
Using selenium results with higher latendcy.


I have used spray http webserver that is based on akka Actor system. which means our Application is not Thread per request, which provids a better scalability and better utiization of resources.

I have used elastic search for indexing the profile and searching for user based on thier name or their skills. elasctic search is scalabale.

***Because both webserver and database is scalabale the application is scalable too, communication is assynchronous***


Elastic search is listening on port **9300** and **9200**


#### rest api:

Searching by user name: 
```bash
/searchbyusername?username=myuser  
```

Searching by kills:
```bash
/searchbyskills?skills=Java%20Oracle%20Scala
```

Indexing a linkedin profile:
```bash
/indexprofile?url=https://www.linkedin.com/in/david-hodeffi-b1b31153
```
